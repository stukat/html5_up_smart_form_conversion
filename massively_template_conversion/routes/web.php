<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Welcome
Route::get('/', function () {
    return view('welcome');
});

// Generic
Route::get('generic', function () {
    return view('generic');
});

// Elements
Route::get('elements', function () {
    return view('elements');
});

Route::post('/usermessage', 'UserMessageController@userMessage');
