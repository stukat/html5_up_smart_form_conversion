<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserMessage;

class UserMessageController extends Controller

{
    public function userMessage() 
    
    {
	    
	    // validate request 
	    
	    $this->validate(request(), [
      	'name'    => 'required',
				'email'   => 'required',
				'message' => 'required'
			]);
			
	  	// Create a new message using the request data	
	  	UserMessage::create(request(['name', 'email', 'message']));

	  	
	  	// Save it to the database
	  	
	  	// Redirect to the homepage
	  	
	  	return redirect('/');
	  	
    }
    
}
